import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Personaje } from '../interfaces/dbz.interface';
import { DbzService } from '../services/dbz.services';

@Component({
  selector: 'app-agregarPersonaje',
  templateUrl: './agregarPersonaje.component.html'
})
export class AgregarPersonajeComponent {

  constructor( private dbzServices: DbzService ) {}

  @Input() nuevo: Personaje = {
    nombre: '',
    poder: 0
  }

  //@Output() onNuevoPersonaje: EventEmitter<Personaje> = new EventEmitter();

  agregar() {
    if( this.nuevo.nombre.trim().length === 0){ return; }
    
    //this.onNuevoPersonaje.emit( this.nuevo )
    this.dbzServices.agregarPersonaje( this.nuevo );
    this.nuevo = { 
      nombre: '',
      poder: 0
     }

  } 

}
