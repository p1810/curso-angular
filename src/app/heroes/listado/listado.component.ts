import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent {
  
    heroes: string[] = ['Superman', 'Hulk','Iroman', 'Thor'];
    heroeBorrado: string = '';

    borrarHeroe(){
      this.heroeBorrado = this.heroes.shift() || '';
    }
}
